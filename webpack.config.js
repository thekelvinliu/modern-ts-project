const { resolve } = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

const ENV = { DEV: 'development', PROD: 'production' };

/**
 * webpack config
 * @param {string} env - build environment
 * @returns {object} webpack config
 */
module.exports = function config(env = ENV.DEV) {
  const isProd = env === ENV.PROD;
  process.env.NODE_ENV = isProd ? ENV.PROD : ENV.DEV;

  return {
    entry: {
      index: resolve('src'),
    },
    resolve: {
      extensions: ['.ts', '.mjs', '.js', '.json'],
    },
    output: {
      filename: `[name]${isProd ? '.[contenthash].' : '.'}js`,
      path: resolve('dist'),
    },
    target: 'node',
    devtool: isProd ? 'source-map' : 'cheap-eval-source-map',
    mode: isProd ? 'production' : 'development',
    module: {
      rules: [
        {
          test: /\.ts$/,
          exclude: /node_modules/,
          use: [
            {
              loader: 'babel-loader',
              options: {
                cacheDirectory: true,
              },
            },
            'ts-loader',
          ],
        },
      ],
    },
    plugins: [new CleanWebpackPlugin()],
  };
};
