module.exports = {
  '*.js': ['yarn lint --fix', 'git add'],
  '*.ts': ['yarn check-types', 'yarn lint --fix', 'git add'],
  '*.{css,html,json,md,scss,yml}': ['yarn format', 'git add'],
};
